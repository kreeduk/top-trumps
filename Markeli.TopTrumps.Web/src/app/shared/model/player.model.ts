﻿import { Card } from "./card.model";

export class Player {
    id: number;
    name: string;
    type: PlayerTypes;
    cards: Card[];

    public constructor(id: number, name: string, type: PlayerTypes) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
}

export enum PlayerTypes {
    Human,
    Computer
}