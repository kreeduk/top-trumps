import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BoardComponent } from './modules/board/board.component';
import { CardService } from './modules/card/card.service';
import { PlayerService } from './modules/player/player.service';
import { ArtificialIntelligenceUtil } from './shared/util/ai.util';
import { CardUtil } from './shared/util/card.util';
import { MathUtil } from './shared/util/math.util';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptionsComponent } from './modules/board/options/options.component';

@NgModule({
    declarations: [
        BoardComponent,
        OptionsComponent
    ],
    providers: [
        CardService,
        PlayerService,
        ArtificialIntelligenceUtil,
        CardUtil,
        MathUtil
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpClientModule
    ],
    bootstrap: [
        BoardComponent
    ]
})
export class AppModule { }