import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MathUtil } from './math.util';
var ArtificialIntelligenceUtil = (function () {
    function ArtificialIntelligenceUtil(mathUtil) {
        this.mathUtil = mathUtil;
    }
    ArtificialIntelligenceUtil.prototype.chooseCardAttribute = function (card) {
        var index = this.mathUtil.getRandomNumber(0, card.attributes.length);
        var attribute = card.attributes[index];
        return attribute;
    };
    ArtificialIntelligenceUtil = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [MathUtil])
    ], ArtificialIntelligenceUtil);
    return ArtificialIntelligenceUtil;
}());
export { ArtificialIntelligenceUtil };
//# sourceMappingURL=ai.util.js.map