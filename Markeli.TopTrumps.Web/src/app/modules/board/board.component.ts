﻿import { Component, ViewChild } from '@angular/core';
import { PlayerService } from '../player/player.service';
import { Player, PlayerTypes } from '../../shared/model/player.model';
import { Card, CardAttribute, CardTypes } from '../../shared/model/card.model';
import { ArtificialIntelligenceUtil } from '../../shared/util/ai.util';
import { CardService } from '../card/card.service';
import { CardUtil } from '../../shared/util/card.util';
import { OptionsComponent } from './options/options.component';

@Component({
    selector: 'app-top-trumps-board',
    templateUrl: './board.component.html'
})

export class BoardComponent  {
    @ViewChild(OptionsComponent)

    optionComponent: OptionsComponent;
    cards: Card[];
    wonMessage = "You won!";
    lostMessage = "You lost!";
    players: Player[];
    currentCard: Card;
    currentAttribute: CardAttribute;
    currentWaitingCard: Card;
    currentPlayer: Player;
    waitingPlayer: Player;
    roundMessage: string;
    round: number;
    showOptions:boolean =  true;

    constructor(private playerService: PlayerService,
        private cardService: CardService,
        private aiUtil: ArtificialIntelligenceUtil,
        private cardUtil: CardUtil) {
    }

    beginGame(): void {
        this.cardService.getByType(CardTypes.Starship).subscribe(response => {this.cards = response});

        const shuffledCards = this.getShuffledCards();

        this.players = this.playerService.getPlayers();
        this.cardUtil.dealCards(shuffledCards, this.players);
        this.round = 1;
        this.currentPlayer = this.players[0];
        this.waitingPlayer = this.players[1];
        this.currentCard = this.currentPlayer.cards[0];
        this.currentWaitingCard = this.waitingPlayer.cards[0];

        if (this.currentPlayer.type === PlayerTypes.Computer) {
            this.computerChoose();
        }
    }

    beginNextRound() {
        const previousCurrentPlayer = this.currentPlayer;
        const previousWaitingPlayer = this.waitingPlayer;

        this.currentPlayer = previousWaitingPlayer;
        this.waitingPlayer = previousCurrentPlayer;
        this.currentCard = this.currentPlayer.cards[0];
        this.currentWaitingCard = this.waitingPlayer.cards[0];
        this.round++;
    }

    compareAttributes(attribute: CardAttribute): void {
        const comparisonAttribute = this.currentWaitingCard.attributes.filter(x => x.key === attribute.key)[0];

        this.winRound(attribute, comparisonAttribute);
        this.loseRound(attribute, comparisonAttribute);
        this.drawRound(attribute, comparisonAttribute);

        if (this.currentPlayer.cards.length === 0 || this.waitingPlayer.cards.length === 0) {
            this.winGame();
        } else {
            this.beginNextRound();
        }
    }

    computerChoose() {
        this.currentAttribute = this.aiUtil.chooseCardAttribute(this.currentCard);
        this.compareAttributes(this.currentAttribute);
    }

    drawRound(attribute: CardAttribute, comparisonAttribute: CardAttribute) {
        if (attribute.value !== comparisonAttribute.value) {
            return;
        }
    }

    getShuffledCards(): Card[] {
        const shuffledCards = this.cardUtil.shuffleCards(this.cards);

        return shuffledCards;
    }

    loseRound(attribute: CardAttribute, comparisonAttribute: CardAttribute) {
        if (attribute.value >= comparisonAttribute.value) {
            return;
        }

        this.waitingPlayer.cards.push(this.currentWaitingCard);
        this.removeTopCard(this.currentPlayer);

        if (this.currentPlayer.type === PlayerTypes.Human) {
            this.roundMessage = this.lostMessage;
        } else {
            this.roundMessage = this.wonMessage;
        }
    }

    removeTopCard(player: Player): void {
        player.cards.shift();
    }

    startButtonClicked() {
        this.showOptions = false;
        this.beginGame();
    }

    winGame() {
        if ((this.currentPlayer.cards.length !== 0 && this.currentPlayer.type === PlayerTypes.Human) ||
            (this.currentPlayer.cards.length !== 0 && this.waitingPlayer.type === PlayerTypes.Human)) {
            //human won game
            return;
        }

        //human lost game
        return;
    }

    winRound(attribute: CardAttribute, comparisonAttribute: CardAttribute) {
        if (attribute.value <= comparisonAttribute.value) {
            return;
        }

        this.currentPlayer.cards.push(this.currentWaitingCard);
        this.removeTopCard(this.waitingPlayer);

        if (this.currentPlayer.type === PlayerTypes.Human) {
            this.roundMessage = this.wonMessage;
        } else {
            this.roundMessage = this.lostMessage;
        }
    }
}