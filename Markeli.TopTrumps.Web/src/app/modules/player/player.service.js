import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Player, PlayerTypes } from '../../shared/model/player.model';
var PlayerService = (function () {
    function PlayerService() {
    }
    PlayerService.prototype.getPlayers = function () {
        var players = [];
        players.push(new Player(1, "Player 1", PlayerTypes.Human));
        players.push(new Player(2, "CPU", PlayerTypes.Computer));
        return players;
    };
    PlayerService = tslib_1.__decorate([
        Injectable()
    ], PlayerService);
    return PlayerService;
}());
export { PlayerService };
//# sourceMappingURL=player.service.js.map