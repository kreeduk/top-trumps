﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class StarshipCardRepository : Repository<Card, Starship>, IStarshipCardRepository
    {
        protected override string Url => "https://swapi.co/api/starships/";

        public StarshipCardRepository(IMapper mapper):base(mapper)
        {
        }
    }
}
