import * as tslib_1 from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
var OptionsComponent = (function () {
    function OptionsComponent() {
        this.startButtonClick = new EventEmitter();
    }
    OptionsComponent.prototype.startButtonClicked = function () {
        this.startButtonClick.emit();
    };
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], OptionsComponent.prototype, "startButtonClick", void 0);
    OptionsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-board-options',
            templateUrl: './options.component.html'
        })
    ], OptionsComponent);
    return OptionsComponent;
}());
export { OptionsComponent };
//# sourceMappingURL=options.component.js.map