﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class PeopleCardRepository : Repository<Card, Starship>, IPeopleCardRepository
    {
        protected override string Url => "https://swapi.co/api/people/";

        public PeopleCardRepository(IMapper mapper) : base(mapper)
        {
        }
    }
}
