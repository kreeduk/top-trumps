﻿using System.Collections.Generic;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Models.Enums;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Services.Abstractions;

namespace Markeli.TopTrumps.Services
{
    public class CardService:ICardService
    {
        private readonly IFilmCardRepository _filmRepository;
        private readonly IPeopleCardRepository _peopleRepository;
        private readonly IPlanetCardRepository _planetRepository;
        private readonly ISpecieCardRepository _specieRepository;
        private readonly IStarshipCardRepository _starshipRepository;
        private readonly IVehicleCardRepository _vehicleRepository;

        public CardService(IFilmCardRepository filmRepository,
            IPeopleCardRepository peopleRepository,
            IPlanetCardRepository planetRepository,
            ISpecieCardRepository specieRepository,
            IStarshipCardRepository starshipRepository, IVehicleCardRepository vehicleRepository)
        {
            _filmRepository = filmRepository;
            _peopleRepository = peopleRepository;
            _planetRepository = planetRepository;
            _specieRepository = specieRepository;
            _starshipRepository = starshipRepository;
            _vehicleRepository = vehicleRepository;
        }

        public IEnumerable<Card> GetByType(CardTypes type)
        {
            switch (type)
            {
                case CardTypes.Films:
                    return _filmRepository.GetAll();
                case CardTypes.People:
                    return _peopleRepository.GetAll();
                case CardTypes.Planets:
                    return _planetRepository.GetAll();
                case CardTypes.Species:
                    return _specieRepository.GetAll();
                case CardTypes.Starship:
                    return _starshipRepository.GetAll();
                case CardTypes.Vehicles:
                    return _vehicleRepository.GetAll();
            }

            return null;
        }
    }
}
