﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class FilmCardRepository : Repository<Card, Starship>, IFilmCardRepository
    {
        protected override string Url => "https://swapi.co/api/films/";

        public FilmCardRepository(IMapper mapper) : base(mapper)
        {
        }
    }
}
