var Player = (function () {
    function Player(id, name, type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
    return Player;
}());
export { Player };
export var PlayerTypes;
(function (PlayerTypes) {
    PlayerTypes[PlayerTypes["Human"] = 0] = "Human";
    PlayerTypes[PlayerTypes["Computer"] = 1] = "Computer";
})(PlayerTypes || (PlayerTypes = {}));
//# sourceMappingURL=player.model.js.map