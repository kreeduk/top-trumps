import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
var CardService = (function () {
    function CardService(http) {
        this.http = http;
    }
    CardService.prototype.getByType = function (type) {
        return this.http
            .get('/api/card/getbytype/' + type)
            .pipe(retry(1), catchError(this.handleError));
    };
    CardService.prototype.handleError = function (error) {
        var errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = 'Error: ${error.error.message}';
        }
        else {
            errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    };
    CardService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], CardService);
    return CardService;
}());
export { CardService };
//# sourceMappingURL=card.service.js.map