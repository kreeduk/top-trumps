﻿using Newtonsoft.Json;

namespace Markeli.TopTrumps.Repositories.Models
{
    public class Starship
    {
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [JsonProperty(PropertyName = "manufacturer")]
        public string Manufacturer { get; set; }

        [JsonProperty(PropertyName = "passengers")]
        public string Passengers { get; set; }

        [JsonProperty(PropertyName = "consumables")]
        public string Consumables { get; set; }

        [JsonProperty(PropertyName = "cargo_capacity")]
        public string CargoCapacity { get; set; }

        [JsonProperty(PropertyName = "cost_in_credits")]
        public string CostInCredits { get; set; }

        [JsonProperty(PropertyName = "crew")]
        public string Crew { get; set; }

        [JsonProperty(PropertyName = "hyperdrive_rating")]
        public string HyperdriveRating { get; set; }

        [JsonProperty(PropertyName = "length")]
        public string Length { get; set; }
    }
}