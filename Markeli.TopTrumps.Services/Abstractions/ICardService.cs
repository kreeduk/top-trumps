﻿using System.Collections.Generic;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Models.Enums;

namespace Markeli.TopTrumps.Services.Abstractions
{
    public interface ICardService
    {
        IEnumerable<Card> GetByType(CardTypes type);
    }
}
