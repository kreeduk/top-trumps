import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import '../../core/extensions/array.extension';
var CardUtil = (function () {
    function CardUtil() {
    }
    CardUtil.prototype.shuffleCards = function (cards) {
        return cards.shuffle();
    };
    CardUtil.prototype.dealCards = function (cards, players) {
        var cardsEach = Math.floor(cards.length / players.length);
        var startIndex = 0;
        players.forEach(function (player) {
            var endIndex = startIndex + cardsEach;
            player.cards = cards.slice(startIndex, endIndex);
            startIndex += cardsEach;
        });
    };
    CardUtil = tslib_1.__decorate([
        Injectable()
    ], CardUtil);
    return CardUtil;
}());
export { CardUtil };
//# sourceMappingURL=card.util.js.map