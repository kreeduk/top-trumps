﻿using System.Collections.Generic;
using Markeli.TopTrumps.Models;

namespace Markeli.TopTrumps.Repositories.Abstractions
{
    public interface IPeopleCardRepository
    {
        IEnumerable<Card> GetAll();
    }
}
