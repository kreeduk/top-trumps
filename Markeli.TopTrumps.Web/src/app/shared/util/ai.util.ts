﻿import { Injectable } from '@angular/core';
import { Card } from '../model/card.model';
import { MathUtil } from './math.util';

@Injectable()
export class ArtificialIntelligenceUtil {
    constructor(private mathUtil: MathUtil) {
    }

    chooseCardAttribute(card: Card) {
        const index = this.mathUtil.getRandomNumber(0, card.attributes.length);
        const attribute = card.attributes[index];

        return attribute;
    }
}