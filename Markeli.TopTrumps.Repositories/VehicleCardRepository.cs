﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class VehicleCardRepository : Repository<Card, Starship>, IVehicleCardRepository
    {
        protected override string Url => "https://swapi.co/api/vehicles/";

        public VehicleCardRepository(IMapper mapper) : base(mapper)
        {
        }
    }
}
