﻿namespace Markeli.TopTrumps.Models
{
    public class CardAttribute
    {
        public string Label { get; set; }
        public string Key { get; set; }
        public decimal Value { get; set; }
    }
}
