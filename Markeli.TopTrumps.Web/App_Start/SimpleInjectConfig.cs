﻿using System.Web.Http;
using AutoMapper;
using Markeli.TopTrumps.Repositories;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Services;
using Markeli.TopTrumps.Services.Abstractions;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace Markeli.TopTrumps.Web
{
    public class SimpleInjectConfig
    {
        public static void RegisterContainers(Container container, MapperConfiguration config)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register(() => config.CreateMapper(container.GetInstance));
            container.Register<IFilmCardRepository, FilmCardRepository>();
            container.Register<IPeopleCardRepository, PeopleCardRepository>();
            container.Register<IPlanetCardRepository, PlanetCardRepository>();
            container.Register<ISpecieCardRepository, SpecieCardRepository>();
            container.Register<IStarshipCardRepository, StarshipCardRepository>();
            container.Register<IVehicleCardRepository, VehicleCardRepository>();

            container.Register<ICardService, CardService>();

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}