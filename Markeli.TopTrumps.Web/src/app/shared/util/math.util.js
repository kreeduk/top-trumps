import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var MathUtil = (function () {
    function MathUtil() {
    }
    MathUtil.prototype.getRandomNumber = function (min, max) {
        var number = Math.floor(Math.random() * max) + min;
        return number;
    };
    MathUtil = tslib_1.__decorate([
        Injectable()
    ], MathUtil);
    return MathUtil;
}());
export { MathUtil };
//# sourceMappingURL=math.util.js.map