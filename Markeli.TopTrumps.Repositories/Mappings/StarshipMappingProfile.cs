﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Models.Enums;
using Markeli.TopTrumps.Repositories.Mappings.Resolvers;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories.Mappings
{
    public class StarshipMappingProfile : Profile
    {
        public StarshipMappingProfile()
        {
            CreateMap<Starship, Card>()
                .ForMember(d => d.CardType, opt => opt.MapFrom(src => CardTypes.Starship))
                .ForMember(d => d.Description, opt => opt.MapFrom(src => src.Model))
                .ForMember(d => d.Attributes, opt => opt.MapFrom<StarshipCardAttributeResolver>());
        }
    }
}
