﻿export interface Card {
    name: string;
    description: string;
    attributes: CardAttribute[];
    cardType: CardTypes;
}

export interface CardAttribute {
    key: string;
    label: string;
    value: number;
}

export enum CardTypes {
    Films,
    People,
    Planets,
    Species,
    Starship,
    Vehicles
}