﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class SpecieCardRepository : Repository<Card, Starship>, ISpecieCardRepository
    {
        protected override string Url => "https://swapi.co/api/species/";

        public SpecieCardRepository(IMapper mapper) : base(mapper)
        {
        }
    }
}
