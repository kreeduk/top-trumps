﻿import { Component, Output, EventEmitter } from '@angular/core';
import { PlayerTypes } from '../../../shared/model/player.model';
import { CardTypes } from '../../../shared/model/card.model';

@Component({
    selector: 'app-board-options',
    templateUrl: './options.component.html'
})

export class OptionsComponent {
    @Output() startButtonClick = new EventEmitter();

    selectedCardType: CardTypes;
    selectedPlayerType: PlayerTypes;

    startButtonClicked(): void {
        this.startButtonClick.emit();
    }
}