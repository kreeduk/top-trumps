﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using AutoMapper;
using Markeli.TopTrumps.Repositories.Models;
using Newtonsoft.Json;

namespace Markeli.TopTrumps.Repositories
{
    public abstract class Repository<TModel, TEntity>
    {
        protected abstract string Url { get; }

        private readonly IMapper _mapper;

        protected Repository(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<TModel> GetAll()
        {
            Response<TEntity> entities;

            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetStringAsync(new Uri(Url)).Result;

                entities = JsonConvert.DeserializeObject<Response<TEntity>>(response);
            }

            return _mapper.Map<List<TModel>>(entities.Results);
        }
    }
}
