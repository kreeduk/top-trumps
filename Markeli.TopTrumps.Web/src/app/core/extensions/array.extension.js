'use strict';
Array.prototype.shuffle = function () {
    var inputArray = this;
    for (var i = inputArray.length - 1; i >= 0; i--) {
        var randomIndex = Math.floor(Math.random() * (i + 1));
        var itemAtIndex = inputArray[randomIndex];
        inputArray[randomIndex] = inputArray[i];
        inputArray[i] = itemAtIndex;
    }
    return inputArray;
};
//# sourceMappingURL=array.extension.js.map