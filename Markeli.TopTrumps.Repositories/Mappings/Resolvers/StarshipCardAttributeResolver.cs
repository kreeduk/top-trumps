﻿using System.Collections.Generic;
using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories.Mappings.Resolvers
{
    public class StarshipCardAttributeResolver : IValueResolver<Starship, Card, IEnumerable<CardAttribute>>
    {
        public IEnumerable<CardAttribute> Resolve(Starship source, Card destination, IEnumerable<CardAttribute> destMember, ResolutionContext context)
        {
            var attributes = new List<CardAttribute>
            {
                new CardAttribute {Key = "Passengers", Label = "Passengers", Value = ConvertToDecimal(source.Passengers)},
                new CardAttribute {Key = "CargoCapacity", Label = "Cargo capacity(kg)", Value = ConvertToDecimal(source.CargoCapacity)},
                new CardAttribute {Key = "CostInCredits", Label = "Cost (Galactic credits)", Value = ConvertToDecimal(source.CostInCredits)},
                new CardAttribute {Key = "Crew", Label = "Crew required", Value = ConvertToDecimal(source.Crew)},
                new CardAttribute {Key = "Length", Label = "Length(m)", Value = ConvertToDecimal(source.Length)}
            };

            return attributes;
        }

        private static decimal ConvertToDecimal(string value)
        {
            decimal.TryParse(value, out var result);

            return result;
        }
    }
}
