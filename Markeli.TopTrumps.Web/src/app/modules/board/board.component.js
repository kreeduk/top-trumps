import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { PlayerService } from '../player/player.service';
import { PlayerTypes } from '../../shared/model/player.model';
import { CardTypes } from '../../shared/model/card.model';
import { ArtificialIntelligenceUtil } from '../../shared/util/ai.util';
import { CardService } from '../card/card.service';
import { CardUtil } from '../../shared/util/card.util';
import { OptionsComponent } from './options/options.component';
var BoardComponent = (function () {
    function BoardComponent(playerService, cardService, aiUtil, cardUtil) {
        this.playerService = playerService;
        this.cardService = cardService;
        this.aiUtil = aiUtil;
        this.cardUtil = cardUtil;
        this.wonMessage = "You won!";
        this.lostMessage = "You lost!";
        this.showOptions = true;
    }
    BoardComponent.prototype.beginGame = function () {
        var _this = this;
        this.cardService.getByType(CardTypes.Starship).subscribe(function (response) { _this.cards = response; });
        var shuffledCards = this.getShuffledCards();
        this.players = this.playerService.getPlayers();
        this.cardUtil.dealCards(shuffledCards, this.players);
        this.round = 1;
        this.currentPlayer = this.players[0];
        this.waitingPlayer = this.players[1];
        this.currentCard = this.currentPlayer.cards[0];
        this.currentWaitingCard = this.waitingPlayer.cards[0];
        if (this.currentPlayer.type === PlayerTypes.Computer) {
            this.computerChoose();
        }
    };
    BoardComponent.prototype.beginNextRound = function () {
        var previousCurrentPlayer = this.currentPlayer;
        var previousWaitingPlayer = this.waitingPlayer;
        this.currentPlayer = previousWaitingPlayer;
        this.waitingPlayer = previousCurrentPlayer;
        this.currentCard = this.currentPlayer.cards[0];
        this.currentWaitingCard = this.waitingPlayer.cards[0];
        this.round++;
    };
    BoardComponent.prototype.compareAttributes = function (attribute) {
        var comparisonAttribute = this.currentWaitingCard.attributes.filter(function (x) { return x.key === attribute.key; })[0];
        this.winRound(attribute, comparisonAttribute);
        this.loseRound(attribute, comparisonAttribute);
        this.drawRound(attribute, comparisonAttribute);
        if (this.currentPlayer.cards.length === 0 || this.waitingPlayer.cards.length === 0) {
            this.winGame();
        }
        else {
            this.beginNextRound();
        }
    };
    BoardComponent.prototype.computerChoose = function () {
        this.currentAttribute = this.aiUtil.chooseCardAttribute(this.currentCard);
        this.compareAttributes(this.currentAttribute);
    };
    BoardComponent.prototype.drawRound = function (attribute, comparisonAttribute) {
        if (attribute.value !== comparisonAttribute.value) {
            return;
        }
    };
    BoardComponent.prototype.getShuffledCards = function () {
        var shuffledCards = this.cardUtil.shuffleCards(this.cards);
        return shuffledCards;
    };
    BoardComponent.prototype.loseRound = function (attribute, comparisonAttribute) {
        if (attribute.value >= comparisonAttribute.value) {
            return;
        }
        this.waitingPlayer.cards.push(this.currentWaitingCard);
        this.removeTopCard(this.currentPlayer);
        if (this.currentPlayer.type === PlayerTypes.Human) {
            this.roundMessage = this.lostMessage;
        }
        else {
            this.roundMessage = this.wonMessage;
        }
    };
    BoardComponent.prototype.removeTopCard = function (player) {
        player.cards.shift();
    };
    BoardComponent.prototype.startButtonClicked = function () {
        this.showOptions = false;
        this.beginGame();
    };
    BoardComponent.prototype.winGame = function () {
        if ((this.currentPlayer.cards.length !== 0 && this.currentPlayer.type === PlayerTypes.Human) ||
            (this.currentPlayer.cards.length !== 0 && this.waitingPlayer.type === PlayerTypes.Human)) {
            return;
        }
        return;
    };
    BoardComponent.prototype.winRound = function (attribute, comparisonAttribute) {
        if (attribute.value <= comparisonAttribute.value) {
            return;
        }
        this.currentPlayer.cards.push(this.currentWaitingCard);
        this.removeTopCard(this.waitingPlayer);
        if (this.currentPlayer.type === PlayerTypes.Human) {
            this.roundMessage = this.wonMessage;
        }
        else {
            this.roundMessage = this.lostMessage;
        }
    };
    tslib_1.__decorate([
        ViewChild(OptionsComponent),
        tslib_1.__metadata("design:type", OptionsComponent)
    ], BoardComponent.prototype, "optionComponent", void 0);
    BoardComponent = tslib_1.__decorate([
        Component({
            selector: 'app-top-trumps-board',
            templateUrl: './board.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [PlayerService,
            CardService,
            ArtificialIntelligenceUtil,
            CardUtil])
    ], BoardComponent);
    return BoardComponent;
}());
export { BoardComponent };
//# sourceMappingURL=board.component.js.map