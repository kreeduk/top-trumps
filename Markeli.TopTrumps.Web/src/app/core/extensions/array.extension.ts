﻿'use strict';
interface Array<T> {
    shuffle(): T[];
}

Array.prototype.shuffle = function (): any[] {
    const inputArray: any[] = this;
    for (let i = inputArray.length - 1; i >= 0; i--) {
        const randomIndex = Math.floor(Math.random() * (i + 1));
        const itemAtIndex: number = inputArray[randomIndex];

        inputArray[randomIndex] = inputArray[i];
        inputArray[i] = itemAtIndex;
    }
    return inputArray;
}