﻿using System.Web.Http;
using Markeli.TopTrumps.Models.Enums;
using Markeli.TopTrumps.Services.Abstractions;

namespace Markeli.TopTrumps.Web.Api
{
    public class CardController : ApiController
    {
        private readonly ICardService _cardService;

        public CardController(ICardService cardService)
        {
            _cardService = cardService;
        }

        [Route("api/card/getbytype/{type}")]
        [HttpGet]
        public IHttpActionResult GetByType(CardTypes type)
        {
            return Ok(_cardService.GetByType(type));
        }
    }
}