﻿using AutoMapper;
using Markeli.TopTrumps.Models;
using Markeli.TopTrumps.Repositories.Abstractions;
using Markeli.TopTrumps.Repositories.Models;

namespace Markeli.TopTrumps.Repositories
{
    public class PlanetCardRepository : Repository<Card, Starship>, IPlanetCardRepository
    {
        protected override string Url => "https://swapi.co/api/planets/";

        public PlanetCardRepository(IMapper mapper) : base(mapper)
        {
        }
    }
}
