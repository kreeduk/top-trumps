﻿import { Injectable } from '@angular/core';
import { Card } from '../model/card.model';
import { Player } from '../model/player.model';
import '../../core/extensions/array.extension';

@Injectable()
export class CardUtil {
    shuffleCards(cards: Card[]): Card[] {
        return cards.shuffle();
    }

    dealCards(cards: Card[], players: Player[]): void {
        const cardsEach = Math.floor(cards.length / players.length);
        var startIndex = 0;

        players.forEach(player => {
            const endIndex = startIndex + cardsEach;

            player.cards = cards.slice(startIndex, endIndex);

            startIndex += cardsEach;
        });
    }
}