﻿const webpack = require("webpack");

module.exports = function () {
    return {
        devtool: "source-map",
        mode: 'development',
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx']
        },
        entry: {
            'polyfills': './src/app/polyfills.ts',
            'main': './src/app/main.ts'
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: ['awesome-typescript-loader', 'angular2-template-loader'],
                    exclude: [/\.(spec|e2e)\.ts$/]
                },
                {
                    test: /\.html$/,
                    use: 'html-loader'
                }
            ]
        },
        output: {
            path: __dirname + '/dist',
            filename: '[name].bundle.js'
        }
    };
}