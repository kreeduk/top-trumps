﻿namespace Markeli.TopTrumps.Models.Enums
{
    public enum CardTypes
    {
        Films,
        People,
        Planets,
        Species,
        Starship,
        Vehicles
    }
}