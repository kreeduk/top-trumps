﻿using System.Collections.Generic;
using Markeli.TopTrumps.Models.Enums;

namespace Markeli.TopTrumps.Models
{
    public class Card
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public CardTypes CardType { get; set; }
        public IEnumerable<CardAttribute> Attributes { get; set; }
    }
}
