﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Markeli.TopTrumps.Repositories.Models
{
    public class Response<T>
    {
        [JsonProperty(PropertyName = "results")]
        public IEnumerable<T> Results { get; set; }
    }
}
