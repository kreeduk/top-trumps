﻿import { Injectable } from '@angular/core';

@Injectable()
export class MathUtil {
    getRandomNumber(min: number, max: number) {
        const number = Math.floor(Math.random() * max) + min;

        return number;
    }
}
