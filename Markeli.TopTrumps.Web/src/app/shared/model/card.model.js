export var CardTypes;
(function (CardTypes) {
    CardTypes[CardTypes["Films"] = 0] = "Films";
    CardTypes[CardTypes["People"] = 1] = "People";
    CardTypes[CardTypes["Planets"] = 2] = "Planets";
    CardTypes[CardTypes["Species"] = 3] = "Species";
    CardTypes[CardTypes["Starship"] = 4] = "Starship";
    CardTypes[CardTypes["Vehicles"] = 5] = "Vehicles";
})(CardTypes || (CardTypes = {}));
//# sourceMappingURL=card.model.js.map