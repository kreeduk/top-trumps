﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Card, CardTypes } from '../../shared/model/card.model';

@Injectable()
export class CardService {
    constructor(private http: HttpClient) {
    }

    getByType(type: CardTypes): Observable<Card[]> {
        return this.http
            .get<Card[]>('/api/card/getbytype/' + type)
            .pipe(
                retry(1),
                catchError(this.handleError));
    }

    private handleError(error: any) {
        let errorMessage = '';

        if (error.error instanceof ErrorEvent) {
            errorMessage = 'Error: ${error.error.message}';
        } else {
            errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
        }

        window.alert(errorMessage);

        return throwError(errorMessage);
    }
}