﻿import { Injectable } from '@angular/core';
import { Player, PlayerTypes } from '../../shared/model/player.model';

@Injectable()
export class PlayerService {
    getPlayers(): Player[] {
        const players: Player[] = [];

        players.push(new Player(1, "Player 1", PlayerTypes.Human));
        players.push(new Player(2, "CPU", PlayerTypes.Computer));

        return players;
    }
}