import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BoardComponent } from './modules/board/board.component';
import { CardService } from './modules/card/card.service';
import { PlayerService } from './modules/player/player.service';
import { ArtificialIntelligenceUtil } from './shared/util/ai.util';
import { CardUtil } from './shared/util/card.util';
import { MathUtil } from './shared/util/math.util';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptionsComponent } from './modules/board/options/options.component';
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                BoardComponent,
                OptionsComponent
            ],
            providers: [
                CardService,
                PlayerService,
                ArtificialIntelligenceUtil,
                CardUtil,
                MathUtil
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                BrowserModule,
                HttpClientModule
            ],
            bootstrap: [
                BoardComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map